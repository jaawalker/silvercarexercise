## iOS Developer Exercise
#### Directions Received

You will be building out the UI for the Locations section of the Silvercar booking flow. You’ll be implementing a table view populated from a locations API.

Reference: ![](iPhone-XR.png)

#### Specifics

**Required:**
- You can use a networking library of your choice to retrieve the results of the locations.
```
[GET] https://api.rac-tst.com/locations
Headers:
Our API handles versioning via headers and you need to use v2 by including the following
header "Api-Version: 2"
Sorting
Items will be sorted by "id" by default. You can specify a sort with the "sort" query parameter 
with value "name". The field name can be optionally prefixed by “-” to indicate descending sort. 
https://api.rac-tst.com/locations?sort=name // ASC
https://api.rac-tst.com/locations?sort=-name // DESC
Pagination
Items will be paginated to 25 items by default. You can specify further pages with the page 
query parmeter.
https://api.rac-tst.com/locations?page=1 
``` 
- Remove locations with a duplicate `multi_car_display_name` - we should only show locations that do not have A4 or Q5 in their `name`.
- If a location has an airport code that is not equal to `null`, the icon should be shown as an airplane. Otherwise, the icon should be a building.
- Use the provided icons
- Location images are fetched using `asset_code` which is returned by the locations API and used to build the following url
```
[GET] https://s3.amazonaws.com/production-silvercar-static-assets/assets/location-assets/{asset_code}_location_3x.jpg"
Example based on asset_code aus
https://s3.amazonaws.com/production-silvercar-static-assets/assets/location-assets/aus_location_3x.jpg
```

**Not Required:**
- Any screens other than the initial screen
- The search functionality

**Bonus Points**
- Support pagination and the fetching of all locations for the screen
- Search functionality
- As much test coverage as possible
- Architecture other than MVC

## To Review Exercise:

- git clone https://jaawalker@bitbucket.org/jaawalker/silvercarexercise.git
* cd ~/Path/To/SilverExercise
*  carthage update --platform iOS --no-use-binaries
* Be amazed!

### Thanks!
```
Thanks for taking the time to review this. If you have any questions please reach out.
Daniel Lyon
``` 
-
