//
//  LocationTableViewController.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/10/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import UIKit
import Alamofire

class LocationTableViewController: UITableViewController, AlertDisplayer {
    
    private enum cellIdentifiers {
        static let basicCell = "basicCell"
    }
    
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    
    var shouldShowLoadingCell = false
    var viewModel: LocationsViewModel!

    let searchController = SESearchControllerViewController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.prefetchDataSource = self
        setUpSearchBar()
        viewModel = LocationsViewModel(delegate: self)
        viewModel.fetchLocations()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController.dismiss(animated: false, completion: nil)
    }
}

extension LocationTableViewController {
    
    func setUpSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.showsCancelButton = false
        searchController.searchBar.placeholder = Constants.searchPlaceholder
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}


extension LocationTableViewController {
    
    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filteredLocations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.basicCell, for: indexPath) as! BasicCell
        let locale = viewModel.filteredLocations[indexPath.row]
        cell.locationViewModel = LocationCellViewModel(locale)
        return cell
    }
}

extension LocationTableViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            viewModel.fetchLocations()
        }
    }
    
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        guard let availableCount = viewModel?.totalAvailable else {
            return false
        }
        return (indexPath.row == viewModel.filteredLocations.count - 1 && viewModel.currentFetchCount < availableCount)
    }
}

extension LocationTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text, !searchText.isEmpty {
            viewModel.filteredLocations = viewModel.locations.filter { locale in
                return (locale.name?.lowercased().contains(searchText.lowercased()) ?? false)
            }
        } else {
            viewModel.filteredLocations = viewModel.locations
        }
        tableView.reloadData()
    }
}

extension LocationTableViewController: LocationsViewModelDelegate {
    
    func onFetchCompleted(with indexPathsToReload: [IndexPath]?) {
            tableView.reloadData()
    }
    
    func onFetchFailed(with reason: String) {
        let title = Constants.warning
        let action = UIAlertAction(title: Constants.ok, style: .default)
        displayAlert(with: title, message: reason, actions: [action])
    }
}

extension LocationTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
