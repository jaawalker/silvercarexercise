//
//  BasicCell.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/14/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class BasicCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var detailImageView: UIImageView!
    
    var locationViewModel: LocationCellViewModel? {
        didSet {
            if let locationViewModel = locationViewModel {
                configure(with: locationViewModel)
            }
        }
    }
    
    override func prepareForReuse() {
        titleLabel.attributedText = nil
        detailLabel.text = String()
        detailImageView.image = nil
        locationImageView.image = nil
    }
    func configure(with locationVM: LocationCellViewModel) {
        titleLabel.attributedText = locationVM.title
        detailLabel.text = locationVM.detail
        detailImageView.image = locationVM.detailImage
        if let imageURL = locationVM.locationImageURL {
            locationImageView.af_setImage(withURL: imageURL, placeholderImage: locationVM.placeHolder)
        }
    }
}
