//
//  ArrayExtensions.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

extension Array {
    
    func removingDuplicates<T: Hashable>(byKey key: (Element) -> T)  -> [Element] {
        var result = [Element]()
        var seen = Set<T>()
        for value in self {
            if seen.insert(key(value)).inserted {
                result.append(value)
            }
        }
        return result
    }
}
