//
//  LocationsViewModel.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation

protocol LocationsViewModelDelegate: class {
    
    func onFetchCompleted(with indexPathsToReload: [IndexPath]?)
    func onFetchFailed(with reason: String)
}

class LocationsViewModel {
    
    weak var delegate: LocationsViewModelDelegate?
    
    var locations: [LocationElement] = [] {
        didSet {
            filteredLocations = locations
        }
    }
    
    var filteredLocations: [LocationElement] = []
    var currentPage = 1
    var totalAvailable: Int?
    var currentFetchCount = 0
    var isFetchInProgress = false
    
    let client = ExersiseClient()
    
    init(delegate: LocationsViewModelDelegate) {
        self.delegate = delegate
    }
    
    func location(at index: Int) -> LocationElement {
        return locations[index]
    }
    
    func fetchLocations() {
        guard !isFetchInProgress else {return}
        isFetchInProgress = true
        client.fetchLocations(page: currentPage) { result in
            switch result {
            case .success(var locales, let total):
                    self.isFetchInProgress = false
                    self.currentFetchCount += locales.count
                    self.totalAvailable = total
                    locales = locales.removingDuplicates(byKey: { $0.multiCarDisplayName })
                    self.locations.append(contentsOf: locales)
                    if self.currentPage > 1 {
                        let indexPathsToReload = self.calculateIndexPathsToReload(from: locales)
                        self.delegate?.onFetchCompleted(with: indexPathsToReload)
                    } else {
                        self.delegate?.onFetchCompleted(with: .none)
                    }
                    self.currentPage += 1
            case .failure(let error):
                DispatchQueue.main.async {
                    self.isFetchInProgress = false
                    self.delegate?.onFetchFailed(with: "\(error)")
                }
            }
        }
    }
    
    func calculateIndexPathsToReload(from newLocations: [LocationElement]) -> [IndexPath] {
        let startIndex = locations.count - locations.count
        let endIndex = startIndex + newLocations.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}
