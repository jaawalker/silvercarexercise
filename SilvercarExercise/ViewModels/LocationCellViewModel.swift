//
//  LocationCellViewModel.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/15/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation
import UIKit

class LocationCellViewModel {
    
    var location: LocationElement
    
    var title: NSAttributedString?
    var detail: String?
    var locationImageURL: URL?
    var detailImage: UIImage?
    let placeHolder = Constants.placeHolder
    
    init(_ locale: LocationElement)  {
        location = locale
        title = locationTitle()
        detail = locale.description
        detailImage = locale.airportCode != nil ? Constants.airplane : Constants.building
        if let code = locale.assetCode {
            locationImageURL = URL(string: String(format: Constants.imageURLString, code))
        }
    }
    
    func locationTitle() -> NSAttributedString {
        let locationName = location.name ?? ""
        let titleFont = UIFont(name: Constants.avenirNextCondensedMedium, size: 21)!
        let attrTitle = NSMutableAttributedString(string: locationName.uppercased(), attributes: [.font : titleFont])
        if let code = location.airportCode {
            let airportCode = " (\(code))"
            let suffixFont = UIFont.systemFont(ofSize: 19, weight: .light)
            let attrAirportCode = NSAttributedString(string: airportCode, attributes: [.font : suffixFont])
            attrTitle.append(attrAirportCode)
        }
        return NSAttributedString(attributedString: attrTitle)
    }
}
