//
//  Address.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

struct Address: Codable {
    
    let id: Int?
    let line1: String?
    let line2: String?
    let city: String?
    let state: String?
    let zip: String?
    let country: Country?
    let latitude: String?
    let longitude: String?
}
