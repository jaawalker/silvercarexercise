//
//  FleetType.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

enum FleetType: String, Codable {
    
    case a4
    case a5c
    case q5
    case q7
}
