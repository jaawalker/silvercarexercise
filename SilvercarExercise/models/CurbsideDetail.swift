//
//  CurbsideDetail.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

struct CurbsideDetail: Codable {
    
    let active: Bool?
    let description: String?
}
