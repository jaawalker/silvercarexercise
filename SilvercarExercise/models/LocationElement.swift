//
//  LocationElement.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/10/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation

struct LocationElement: Codable {
    
    let id: Int?
    let name: String?
    let airportCode: String?
    let phoneNumber: String?
    let textNumber: String?
    let fleetType: FleetType?
    var multiCarDisplayName: String?
    let openDate: String?
    let closeDate: String?
    let gdsCode: String?
    let deliverable: Bool?
    let deliverableDescription: DeliverableDescription?
    let description: String?
    let timeZone: String?
    let bookable: Bool?
    let hours: String?
    let assetCode: String?
    let address: Address?
    let curbsideDetail: CurbsideDetail?
}

extension LocationElement {
    
    // MARK: - CodingKeys
    enum CodingKeys : String, CodingKey {
        
        case id
        case name
        case airportCode = "airport_code"
        case phoneNumber = "phone_number"
        case textNumber = "text_number"
        case fleetType = "fleet_type"
        case multiCarDisplayName = "multi_car_display_name"
        case openDate = "open_date"
        case closeDate = "close_date"
        case gdsCode = "gds_code"
        case deliverable
        case deliverableDescription = "deliverable_description"
        case description
        case timeZone = "time_zone"
        case bookable
        case hours
        case assetCode = "asset_code"
        case address
        case curbsideDetail = "curbside_detail"
    }
}
