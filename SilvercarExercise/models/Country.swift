//
//  Country.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//


enum Country: String, Codable {
    
    case US
}

extension Country {
    
    // MARK: - CodingKeys
    enum CodingKeys : String, CodingKey {
        
        case us = "US"
    }
}
