//
//  Constants.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    // MARK: - Strings
    static let searchPlaceholder = "Search Locations"
    static let done = "Done"
    static let basicCell = "basicCell"
    static let apiVersion = ["Api-Version":"2"]
    static let locationsBase = "https://api.rac-tst.com/locations"
    static let defaultParameters = ["sort": "city"]
    static let page = "page"
    static let warning = "Warning"
    static let ok = "OK"
    static let total = "Total"
    static let avenirNextCondensedMedium = "AvenirNextCondensed-Medium"
    static let imageURLString = "https://s3.amazonaws.com/production-silvercar-static-assets/assets/location-assets/%@_location_3x.jpg"
    
    // MARK: - CGFloat
    static let fifty: CGFloat = 50

    // MARK: - images
    static let airplane = #imageLiteral(resourceName: "airplane")
    static let building = #imageLiteral(resourceName: "building")
    static let placeHolder = #imageLiteral(resourceName: "placeHolder")
}
