//
//  SESearchControllerViewController.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/10/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import UIKit

class SESearchControllerViewController: UISearchController {
    
    let customSearchBar = SESearchBar()
    
    override var searchBar: UISearchBar { return customSearchBar }
}

class SESearchBar: UISearchBar {
    
    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
        var searchBarTextField: UITextField?
        for subView in self.subviews {
            for sndSubView in subView.subviews {
                if let textField = sndSubView as? UITextField {
                    searchBarTextField = textField
                    break
                }
            }
        }
        searchBarTextField?.doneAccessory = true
    }
}

extension UITextField {
    
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Constants.fifty))
        doneToolbar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: Constants.done , style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}
