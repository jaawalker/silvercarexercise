//
//  ExersiseClient.swift
//  SilvercarExercise
//
//  Created by Daniel Lyon on 3/13/19.
//  Copyright © 2019 Daniel Lyon. All rights reserved.
//

import Alamofire
import UIKit

class ExersiseClient {
    
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    enum Result<T> {
        case success(T, Int)
        case failure(Error)
    }
    
    func fetchLocations(page: Int, completion: @escaping (Result<[LocationElement]>) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let headers = Constants.apiVersion
        var parameters = Constants.defaultParameters
        parameters [Constants.page] = "\(page)"
        Alamofire.request(Constants.locationsBase,
                          method:.get,
                          parameters: parameters,
                          encoding: URLEncoding(destination: .queryString),
                          headers: headers ).responseJSON { response in
                            switch response.result {
                            case .failure(let error):
                                completion(.failure(error))
                            case .success( _):
                                let totalAvailableString = response.response?.allHeaderFields[Constants.total] as? String
                                let totalAvailable = Int(totalAvailableString ?? "") ?? 0
                                if let data = response.data {
                                    do {
                                        let locations = try  JSONDecoder().decode([LocationElement].self, from: data)
                                        completion(.success(locations, totalAvailable))
                                    } catch {
                                        completion(.failure(error))
                                    }
                                }
                            }
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            
        }
    }
}
